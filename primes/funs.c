// Returns the greatest prime number below x
int prime_number(int x) {
  int i = 2;
  while (i <= x/2) {
	if (x % i == 0) {
		return prime_number(x-1);
	}
	i = i+1;
  }

  return x;
}

int fun2(int x) {
  return x+1;
}

int factorial(int x) {
  if (x == 0)
    return 1;
  return x * factorial(x-1);
}
