-module(main).
-export([start/1, stop/0, init/1]).
-export([prime_number_c/1, fun2/1, test/1, prime_number_erlang/1]).

start(ExtPrg) -> spawn(?MODULE, init, [ExtPrg]).
stop() -> complex ! stop.

prime_number_c(X) -> call_port({prime_number, X}).
fun2(Y) -> call_port({fun2, Y}).

call_port(Msg) ->
	complex ! {call, self(), Msg},
  receive
		{complex, Result} -> Result
  end.

init(ExtPrg) ->
	register(complex, self()),
  process_flag(trap_exit, true),
  Port = open_port({spawn, ExtPrg}, [{packet, 2}, binary]),
  loop(Port).

loop(Port) ->
  receive
		{call, Caller, Msg} -> Port ! {self(), {command, term_to_binary(Msg)}},
			receive
				{Port, {data, Data}} -> Caller ! {complex, binary_to_term(Data)}
	    end,
	    loop(Port);
		stop -> Port ! {self(), close},
	    receive
		    {Port, closed} -> exit(normal)
	    end;
	  {'EXIT', Port, Reason} ->
      io:format("~p ~n", [Reason]),
      exit(port_terminated)
  end.

prime_number_erlang(1) -> -1;
prime_number_erlang(Number) ->
	case find_divisor(Number, 2) of
		prime -> Number;
		notprime -> prime_number_erlang(Number-1)
end.
	

find_divisor(Number, Count) ->
	if (Count > (Number div 2)) -> prime;
	true ->
		case (Number rem Count) of
			0 -> notprime;
			_ -> find_divisor(Number, Count+1)
		end
end.

test(Number) ->
	{TimeErlang, ValueErlang} = timer:tc(?MODULE, prime_number_erlang, [Number]),
	{TimeCPort, ValueC} = timer:tc(?MODULE, prime_number_c, [Number]),
	TimeDifference = erlang:abs(TimeErlang - TimeCPort),
	io:format("-----------------------------~n"),
	io:format("Time erlang: ~w ms~n", [TimeErlang div 1000]),
	io:format("Time C:      ~w ms~n", [TimeCPort div 1000]),
	io:format("Difference:  ~w ms~n", [TimeDifference div 1000]),
	io:format("Erlang value: ~w~n", [ValueErlang]),
	io:format("C value:       ~w~n", [ValueC]),
	io:format("-----------------------------~n").


































