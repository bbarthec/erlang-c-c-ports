-module(example3_).
-export([start/1, stop/0, init/1]).
-export([foo/1, bar/1]).

start(SharedLib) ->
  case erl_ddll:load_driver(".", SharedLib) of
    ok -> ok;
    {error, already_loaded} -> ok;
    _ -> exit({error, could_not_load_driver})
  end,
  spawn(?MODULE, init, [SharedLib]).

stop() -> complex3_ ! stop.

init(SharedLib) ->
  register(complex3_, self()),
  Port = open_port({spawn, SharedLib}, []),
  loop(Port).

loop(Port) ->
  receive
    {call, Caller, Msg} ->
      Port ! {self(), {command, encode(Msg)}},
      receive
        {Port, {data, Data}} ->
          Caller ! {complex3_, decode(Data)}
      end,
      loop(Port);
    stop ->
      Port ! {self(), close},
      receive
        {Port, closed} ->
          exit(normal)
      end;
    {'EXIT', Port, Reason} ->
      io:format("~p ~n", [Reason]),
      exit(port_terminated)
  end.

encode({foo, X}) -> [1, X];
encode({bar, Y}) -> [2, Y].
decode([Int]) -> Int.

foo(X) when X < 0 -> {error, "Wrong input value -> [0,255]"};
foo(X) when X > 255 -> {error, "Wrong input value -> [0,255]"};
foo(X) -> call_port({foo, X}).
bar(Y) when Y < 0 -> {error, "Wrong input value -> [0, 128]"};
bar(Y) when Y > 128 -> {error, "Wrong input value -> [0, 128]"};
bar(Y) -> call_port({bar, Y}).

call_port(Msg) ->
  complex3_ ! {call, self(), Msg},
  receive
    {complex3_, Result} ->
      Result
  end.