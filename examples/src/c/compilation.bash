#!/bin/bash

gcc -o example1 example.c erl_comm.c port.c
gcc -o example2 -I/usr/lib/erlang/lib/erl_interface-3.8/include -L/usr/lib/erlang/lib/erl_interface-3.8/lib example.c erl_comm.c ei.c -lerl_interface -lei -lpthread
gcc -o ../driver.so -fpic -shared example.c port_driver.c
